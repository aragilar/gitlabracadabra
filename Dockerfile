FROM python:3-slim-bookworm as build

WORKDIR /app
COPY . /app

RUN pip install --no-cache-dir -r build-requirements.txt && \
    python3 -m build --outdir /tmp/out && \
    cp urllib3-Lower-retry-message-from-WARNING-to-INFO.patch /tmp/out/

# ===================================
FROM debian:bookworm-slim

# DL3008 Pin versions in apt get install
# hadolint ignore=DL3008
RUN --mount=type=cache,from=build,source=/tmp/out,target=/tmp/out \
    set -x && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
      python3-github \
      python3-gitlab \
      python3-html5lib \
      python3-jsonschema \
      python3-packaging \
      python3-pygit2 \
      python3-semantic-version \
      python3-yaml \
      python3-pip \
      \
      patch \
    && \
    patch /usr/lib/python3/dist-packages/urllib3/connectionpool.py < /tmp/out/urllib3-Lower-retry-message-from-WARNING-to-INFO.patch && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge -y \
      patch \
    && \
    apt-get -y autoremove && \
    adduser --disabled-password --gecos '' gitlabracadabra && \
    pip install --no-cache-dir --break-system-packages /tmp/out/gitlabracadabra-*.whl

USER gitlabracadabra

ENTRYPOINT ["gitlabracadabra"]
