# Gitlabracadabra <!-- omit in toc -->

🧹 GitLabracadabra 🧙

:alembic: Adds some magic to GitLab :crystal\_ball:

GitLab'racadabra is a GitOps way to configure a [GitLab](https://gitlab.com/) instance
from a YAML configuration, using the [API](https://docs.gitlab.com/ce/api/README.html).

It is able to create GitLab's [groups](doc/group.md), [projects](doc/project.md),
[users](doc/user.md) and [application settings](doc/application_settings.md).

:thumbsup: It's also able to mirror :

- Git repositories, using the `mirrors` parameter in [Mirroring repositories](doc/project.md#mirroring-repositories).
- container (Docker) images, using the `image_mirrors` parameter. See [Mirroring container images](doc/image_mirrors.md).
- packages, using the `package_mirrors` parameter. See [Mirroring packages](doc/package_mirrors.md).

It is based on [Python GitLab](https://github.com/python-gitlab/python-gitlab).

## Table of Contents <!-- omit in toc -->

- [Installation](#installation)
  - [Using Debian packages](#using-debian-packages)
  - [Using pip](#using-pip)
  - [Using docker image](#using-docker-image)
  - [From source](#from-source)
- [Configuration](#configuration)
- [Action file(s)](#action-files)
- [Using gitlabracadabra in GitLab CI](#using-gitlabracadabra-in-gitlab-ci)
- [Contributing](#contributing)

## Installation

### Using Debian packages

Debian package is available [from artifacts](https://gitlab.com/gitlabracadabra/gitlabracadabra/-/jobs/artifacts/main/browse/debian/output?job=build-deb) and can be installed with:

```shell
apt install gitlabracadabra_*.deb

gitlabracadabra --verbose --dry-run
```

Note: Debian 11 bullseye or later is required.

### Using pip

```shell
pip install gitlabracadabra
```

### Using docker image

There are also [Docker/OCI images](https://gitlab.com/gitlabracadabra/gitlabracadabra/container_registry).

Example usage:

```shell
sudo docker run -ti \
  -v "$HOME/.python-gitlab.cfg:/home/gitlabracadabra/.python-gitlab.cfg:ro" \
  -v "$PWD/gitlabracadabra.yml:/app/gitlabracadabra.yml:ro" \
  'registry.gitlab.com/gitlabracadabra/gitlabracadabra:v1.13.0' \
  --verbose --dry-run
```

Other images are available. Examples:

- `registry.gitlab.com/gitlabracadabra/gitlabracadabra/main`: Current `main`
- `registry.gitlab.com/gitlabracadabra/gitlabracadabra/main:b1cd3482bf9583c5db863c359e12cafcdb7119bf`: A specific commit of `main`

### From source

Local installation (in `$HOME/.local`):

```shell
# On Debian or Ubuntu
sudo apt install -y --no-install-recommends \
  python3-build \
  python3-github \
  python3-gitlab \
  python3-html5lib \
  python3-jsonschema \
  python3-packaging \
  python3-pygit2 \
  python3-semantic-version \
  python3-vcr \
  python3-venv \
  python3-yaml \
  python3-pip \
  python3-coverage \
  python3-pytest
# On Alpine
sudo apk add \
  py3-build \
  py3-pygithub \
  py3-requests-toolbelt \
  py3-html5lib \
  py3-jsonschema \
  py3-packaging \
  py3-pygit2 \
  py3-semantic-version \
  py3-vcrpy \
  py3-yaml \
  py3-pip \
  py3-coverage \
  py3-pytest
# On others
pip install build

# Build and install
python3 -m build
version="$(grep __version__ gitlabracadabra/__init__.py  | awk -F "'" '{print $2}')"
pip install --user "dist/gitlabracadabra-$version"*.whl

# Test
pytest-3  # or pytest
~/.local/bin/gitlabracadabra --verbose --dry-run
```

## Configuration

GitLabracadabra uses the same configuration file as Python GitLab CLI to store
connection parameters.

Example `~/.python-gitlab.cfg`:

```ini
[global]
default = gitlab

[gitlab]
url = https://gitlab.com
private_token = T0K3N
```

More information in [Python GitLab documentation](https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#content).

## Action file(s)

GitLabracadabra *actions* are configured with a YAML file.

See [GitLabracadabra's own action file](https://gitlab.com/gitlabracadabra/gitlabracadabra/blob/main/gitlabracadabra.yml)
or read:

- [Action file syntax](doc/action_file.md)
- list of parameters:
  - [for projects](doc/project.md)
  - [for groups](doc/group.md)
  - [for users](doc/user.md)
  - [for application settings](doc/application_settings.md)

## Using gitlabracadabra in GitLab CI

Since job token probably won't have enough permissions, you'll need to use a personal access token:

- [create a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
- [Define](https://docs.gitlab.com/ee/ci/variables/README.html#create-a-custom-variable-in-the-ui) the
`GITLAB_PRIVATE_TOKEN` **protected** variable
- Use it in your jobs to configure `python-gitlab`. Example `.gitlab-ci.yml`:

```yaml
default:
  image:
    name: 'registry.gitlab.com/gitlabracadabra/gitlabracadabra:v1.13.0'
    entrypoint: [""]
  before_script:
    - |
        cat << EOF > ~/.python-gitlab.cfg
        [global]
        default = gitlab
        [gitlab]
        url = ${CI_SERVER_URL:-https://gitlab.com}
        private_token = ${GITLAB_PRIVATE_TOKEN}
        # job_token = ${GITLAB_JOB_TOKEN}
        EOF

stages:
  - test
  - deploy

test:
  stage: test
  script:
    - gitlabracadabra --verbose --dry-run
  rules:
    - if: '$CI_COMMIT_BRANCH != "main"'

apply:
  stage: deploy
  script:
    - gitlabracadabra --verbose
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
```

## Contributing

- File bugs and feature requests in
  [GitLab issues](https://gitlab.com/gitlabracadabra/gitlabracadabra/-/issues).
  Security issues should be marked as **confidential**.
- Propose documentation or code improvements in
  [GitLab merge requests](https://gitlab.com/gitlabracadabra/gitlabracadabra/-/merge_requests).

  This repository enforces commit message convention, to check this locally install the
  [commitlint](https://github.com/conventional-changelog/commitlint/#what-is-commitlint)
  hook:

  ```shell
  npm install  @commitlint/{config-conventional,cli}
  echo 'npx commitlint --edit' >> .git/hooks/commit-msg
  chmod +x .git/hooks/commit-msg
  ```

See also:

- Setting up [a development environment](doc/dev_setup.md)
- [Configuring VS code](doc/vscode.md)
- [Releasing GitLabracadabra](doc/release.md).
